<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController{

    
    public function index()
    {
        $posts= Post::get();

        return view('posts.index', compact('posts'));
    }
    public function show ($postid){
        $posts= Post::findorfail($postid);
           # dd($post);
        return view('posts.show', ['post'=>$posts]);
    }

    public function create(){

        return view('posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=> ['required','min:4'],
            'body' => ['required','min:4'],
        ]);

        $posts = new Post;
        $posts->title =$request->input('title');
        $posts->body  =$request->input('body');
        $posts->save();

        session()->flash('status','Post created');

        return to_route('posts.index');
    }
    public function edit( Post $posts){
        return view('posts.edit',['posts'=> $posts]);
    }
    
    public function update(Request $request, $posts){

        $request->validate([
            'title'=> ['required','min:4'],
            'body' => ['required','min:4'],
        ]);

        $posts->title =$request->input('title');
        $posts->body  =$request->input('body');
        $posts->save();

        session()->flash('status','Post Update');

        return view('posts.show',compact('posts'));
    }
}