<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;


Route::view('/', 'welcome')->name('home');
Route::view('/contact', 'contact')->name('contact');
Route::get('/posts.index',[PostController::class, 'index'])->name('posts.index');
Route::get('/blog/create',[PostController::class, 'create'])->name('posts.create');
Route::post('/blog',[PostController::class, 'store'])->name('posts.store');
Route::get('/blog/{postid}',[PostController::class, 'show'])->name('posts.show');
Route::get('/blog/{postid}/edit',[PostController::class, 'edit'])->name('posts.edit');
Route::put('/blog/{posts}',[PostController::class,'update'] )->name('posts.update');
Route::view('/about', 'about')->name('about');