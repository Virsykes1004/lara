<x-layouts.app 

:title="$posts->title" 
:meta-description="$posts->body" 
>  

<h1>Edit Form</h1>

<form action="{{route('posts.update' , $posts) }}" method="POST">
     @method('PUT')
     @csrf
    <label >
        Title <br>
        <input name="title" type="text" value="{{old('title', $posts->title)}}">

         @error('title')
        <br>
            <small style="color: red" >{{ $message }}</small>           
        @enderror

    </label><br>
    <label >
        Body <br>
        <textarea name="body" >{{old('body' , $posts->body)}}</textarea>
        
        @error('body')
        <br>
            <small style="color: red" >{{ $message }}</small>
            
        @enderror
    </label><br>
    <button type="submit">Enviar</button>
    <br>

</form>
<br>
<a href="{{ route('posts.index') }}">Regresar</a>

</x-layouts.app>